import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { HttpHeaders, HttpClient, HttpParams } from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class TeamService {

  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*"
    })
  };
  constructor(private http: HttpClient) { }
  private formatErrors(error: any) {
    return throwError(error.error);
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(path, { params }).pipe(catchError(this.formatErrors));
  }
  put(path: string, body: Object = {}): Observable<any> {
    return this.http
      .put(path, JSON.stringify(body), this.httpOptions)
      .pipe(catchError(this.formatErrors));
  }

  addTeam(order_dto: any): Observable<any> {
    return this.http.post("http://localhost:3000/team"  ,order_dto )
}

showTeam(){
  return this.get("http://localhost:3000/team");
}
}
