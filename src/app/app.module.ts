import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { ListTeamComponent } from './list-team/list-team.component';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './header/header.component';



const appRoots : Routes=[

  {path: 'teams', component: ListTeamComponent},
  {path: '', component: HomeComponent},

  ]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListTeamComponent,
    HeaderComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,

    RouterModule.forRoot(appRoots)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
