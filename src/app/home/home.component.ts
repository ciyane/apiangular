import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { combineAll } from 'rxjs/operators';
import { Team } from '../Models/team';
import { TeamService } from '../team.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  TeamForm: FormGroup | undefined;
  fileIsUploading = false;
  order_dto : Team | undefined;
  teams : Team[] | undefined;

  alert: boolean = false;
  kaka: any;
  group1Name: any[];
  //fileUrl : string;
  constructor(private formBuilder : FormBuilder , private service: TeamService , private route : Router ) { }

  ngOnInit(){


    this.onviewTeams();
    this.initForm();


  }
  initForm(){
    this.TeamForm= this.formBuilder.group({
      team: ['', Validators.required ]

    })
  }



  onviewTeams(){
    this.service.showTeam().subscribe(data=>{
this.teams  = data;

console.log(data)
    })
  }







  onSaveTeam(){

    this.order_dto={
      id : 0 ,
      team: this.TeamForm.get('team')?.value
    }
    console.log(this.order_dto.team);
    this.group1Name= this.teams.map(function(p: { team: any; }){
      return p.team;
       })


if(!this.group1Name.includes(this.order_dto.team)){

    this.service.addTeam(this.order_dto).subscribe((data)=>{
            this.teams =data;
            this.ngOnInit();
          })

        }else{
          Swal.fire(this.order_dto.team  +' this team already exists');
        }



  }






}
